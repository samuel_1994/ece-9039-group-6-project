# ECE 9039 Machine Learning Project
# Group 6: Tao Xu, Xu Zhang, Zhi Wang

# Outliers Detection by using Box Plot

import pandas as pd
import matplotlib.pyplot as plt

data = pd.read_csv("EPL 2000-2018.csv")
test_data=data.iloc[:,0:10]

plt.rcParams['axes.unicode_minus']=False
plt.figure(1,figsize=(10,10))
p=data.boxplot(return_type='both')
x = p['fliers'][0].get_xdata()
y = p['fliers'][0].get_ydata()
y.sort()
for i in range(len(x)):
    if i>0:
        plt.annotate(y[i],xy=(x[i],y[i]),xytext=(x[i]+0.05 -0.8/(y[i]-y[i-1]),y[i]))
    else:
        plt.annotate(y[i],xy=(x[i],y[i]),xytext=(x[i]+0.08,y[i]))
plt.show()