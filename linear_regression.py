# ECE 9039 Machine Learning Project
# Group 6: Tao Xu, Xu Zhang, Zhi Wang

# Linear Regression modelling

import pandas as pd
import matplotlib.pyplot as plt
import numpy as np
from sklearn import linear_model
from sklearn.model_selection import train_test_split
from sklearn.metrics import r2_score

data_train = pd.read_csv("EPL 2000-2018.csv",na_filter = False)
x = data_train[['HTHG','HTAG','HH']]
y = data_train[['H','A','D']]
x_train,x_test,y_train,y_test = train_test_split(x,y,train_size=0.8,test_size=0.2, random_state = 42)

# Train the model using training sets
Lin_reg = linear_model.LinearRegression()
Lin_reg.fit(x_train,y_train)

# Predict
Y = Lin_reg.predict(x_test)

# Accuarcy
A_reg = Lin_reg.score(x_test,y_test)
print('Accuarcy',A_reg)

# Score
score=r2_score(y_test,Y)
print('score:',score)

# Plot
plt.scatter(x_test,Y,label='predict')
plt.scatter(x_test,y_test,label='test')
# Plot title 
plt.title("Area realationship with Room number") 
# Function to show plot 
plt.show()