# ECE 9039 Machine Learning Project
# Group 6: Tao Xu, Xu Zhang, Zhi Wang

# Feature Selection

import pandas as pd
import numpy as np
from sklearn.ensemble import ExtraTreesClassifier
import matplotlib.pyplot as plt

data = pd.read_csv("EPL 2000-2018.csv",na_filter = False)
# Independent columns
X = data.iloc[:,0:15]
# Target column
y = data.iloc[:,16:19]

model = ExtraTreesClassifier()
model.fit(X,y)

# Use inbuilt class feature_importances of tree based classifiers
print(model.feature_importances_)

# Plot graph of feature importances for better visualization
feat_importances = pd.Series(model.feature_importances_, index=X.columns)
feat_importances.nlargest(7).plot(kind='barh')
plt.show()